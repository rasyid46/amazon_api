from django.db import models

# Create your models here.

class ProductDetailModel(models.Model):
    brand = models.CharField(max_length=25)

    def __str__(self) -> str:
        return self.brand

class ProductsModel(models.Model):
    name = models.CharField(max_length=255)
    product_link = models.URLField(unique=True)
    price = models.DecimalField(decimal_places=2, max_digits=2)
    uuid = models.UUIDField()
    asin = models.CharField(max_length=255)
    product_image_url = models.URLField
    product_detail = models.OneToOneField(ProductDetailModel, on_delete=models.CASCADE)

    class Meta:
        verbose_name ='Product'
        verbose_name_plural ='Products'

    def __str__(self) -> str:
        return self.name
