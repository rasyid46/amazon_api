from django.contrib import admin

# Register your models here.

from apps.products.models import*

class ProductsModelAdmin(admin.ModelAdmin):
    pass


admin.site.register(ProductsModel, ProductsModelAdmin)